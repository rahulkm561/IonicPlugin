import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the ServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceProvider {
   data: any;
  constructor(public http: Http) {
    this.data = null;
  }
  
 load() {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    // don't have the data yet
    let body = JSON.stringify({'uuid':'31203-21313-3123-4254', 'major':1, 'id':'3', 'name': 'morpheus','job': 'leader'});
    let headers = new Headers();
    headers.append('x-auth-token', 'kdklmdkml');
    headers.append('Content-Type', 'application/json');

    return new Promise(resolve => {
      // We're using Angular Http provider to request the data,
      // then on the response it'll map the JSON data to a parsed JS object.
      // Next we process the data and resolve the promise with the new data.
      this.http.post('https://reqres.in/api/users', body, {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
		  
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          //this.data = data;
          resolve(this.data);
        });
    });
	 
  }
  
  getUsers() {
	  
  return new Promise(resolve => {
    this.http.get('https://reqres.in/api/users?page=2').map(res => res.json())
        .subscribe(data => {
          this.data = data.data;
		  
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          //this.data = data;
          resolve(this.data);
        });
  });
}
  
}
