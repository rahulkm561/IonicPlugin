import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Platform, Events } from 'ionic-angular';
import { NgZone } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
// providers
import { BeaconProvider } from '../../providers/beacon/beacon-provider'
//import {Observable} from 'rxjs/Rx';
// models
import { BeaconModel } from '../../models/beacon-model';
import { ServiceProvider } from '../../providers/service/service';
import { BackgroundMode } from '@ionic-native/background-mode';
import { NativeAudio } from '@ionic-native/native-audio';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
beacons: BeaconModel[] = [];
zone: any;
public people: any;
users: any;
  constructor(public restProvider: ServiceProvider,public navCtrl: NavController,public platform: Platform, public beaconProvider: BeaconProvider, public events: Events, public toastCtrl: ToastController,public nativeAudio: NativeAudio , public backgroundMode : BackgroundMode) {
    this.zone = new NgZone({ enableLongStackTrace: false });
	
	this.nativeAudio.preloadSimple('audio1', 'audio/1.mp3').then((msg)=>{
  console.log("message: " + msg);
}, (error)=>{
  console.log("error: " + error);
});
   this.getUsers();  
   this.loadPeople();
   
  }
ionViewDidLoad() {
	this.favoriteRecipe();
    this.platform.ready().then(() => {
      this.beaconProvider.initialise().then((isInitialised) => {
        if (isInitialised) {
          this.listenToBeaconEvents();
        }
      });
    });
  }

  listenToBeaconEvents() {
    this.events.subscribe('didRangeBeaconsInRegion', (data) => {
       
      // update the UI with the beacon list  
      this.zone.run(() => {

        this.beacons = [];

        let beaconList = data.beacons;
        beaconList.forEach((beacon) => {
          let beaconObject = new BeaconModel(beacon);
          this.beacons.push(beaconObject);
        });

      });

    });
  }
  
  favoriteRecipe() {
  //this.favorite = true;
  let toast = this.toastCtrl.create({
    message: `Added to your favorites!`,
    duration: 200
  });
  toast.present();
 
}



public playAudio(){
    this.backgroundMode.enable();
    this.backgroundMode.on("activate").subscribe(()=>{
      this.nativeAudio.play("audio1"); 
      this.favoriteRecipe();	  
    });
    //this.nativeAudio.play("audio1"),() => console.log('audio1 is done playing'));
  }

loadPeople(){
    this.restProvider.load()
    .then(data => {
      this.people = data;
	
    });
	
  }
  
  getUsers() {
    this.restProvider.getUsers()
    .then(data => {
      this.users = data;
      console.log('Data'+this.users);
    });
  }

}

